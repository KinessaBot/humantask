﻿using System;

namespace HumanApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Boy boy = new Boy(false, false, 178, "Daboy", 17);
            Girl girl = new Girl(true, true, 165, "Dagirl", 17);

            Console.WriteLine(girl.ToString());
            Console.WriteLine(boy.ToString());

            girl.Sew();
            girl.Knit();

            boy.Shave();
            boy.Hack();

            Human human =  new Human(180,"dahuman",17);
            Console.WriteLine(human.ToString());

            Human[] humans = { boy, girl, human };
            foreach (Human a in humans)
                Console.WriteLine(a.Ages);

            Console.ReadLine();

        }
    }
}
