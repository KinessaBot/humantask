﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HumanApp
{
    class Girl : Human
    {
        public bool makeup { get; set; }
        public bool pigtails { get; set; }
        override public int Ages {
            get
            {
                 return ages > 2 ? ages - 2 : ages;
            }
            set
            {
               ages = value;
            }
        }

        public Girl(bool makeup, bool pigtails, float height, string name, int ages): base(height, name, ages)
        {
            this.makeup = makeup;
            this.pigtails = pigtails;
        }

        public void Sew()
        {
            Console.WriteLine(Name + " sewing things");
        }

        public void Knit()
        {
            Console.WriteLine(Name + " kniting things");
        }

        public override string ToString()
        {
            return base.ToString() + " " + makeup.ToString() + " " + pigtails.ToString();
        }
    }
}
