﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HumanApp
{
    class Boy : Human
    {
        public bool beard { get; set; }
        override public int Ages
        {
            get
            {
                return ages + 2;
            }
            set
            {
                ages = value;
            }
        }

        public bool bruise { get; set; }

        public Boy(bool beard, bool bruise, float height, string name, int ages) : base(height, name, ages)
        {
            this.beard = beard;
            this.bruise = bruise;
        }

        public void Hack()
        {
            Console.WriteLine(Name + " hacking trees");
        }

        public void Shave()
        {
            Console.WriteLine(Name + " shaving his face");
        }

        public override string ToString()
        {
            return base.ToString() + " " + beard.ToString() + " " + bruise.ToString();
        }
    }
}
