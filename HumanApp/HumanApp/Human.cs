﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HumanApp
{
    class Human
    {
        public float Height { get; set; }
        public string Name { get; set; }
        protected int ages;
        virtual public int Ages
        {
            get
            {
                return ages;
            }

            set
            {
                ages = value;
            }
        }

        public Human(float height, string name, int ages)
        {
            Height = height;
            Name = name;
            Ages = ages;
        }

        public override string ToString()
        {
            return base.ToString()+" "+Height.ToString()+ " " + Name + " " +Ages.ToString();
        }
    }
}
